# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

require 'trello'
Trello.configure do |config|
  config.developer_public_key = '39dd1232d7fe8b4e38fe7e612f0ce2ba' # The "key" from step 1
  config.member_token = '3fab269b825d85f5aaf37162a91e8f964ac003fe4392b692728a0098136873c9' # The token from step 2.
end